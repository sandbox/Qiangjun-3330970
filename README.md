# CKEditor5 Extras

For example, disable the autoformat plugin:

```php
// Add the line below into your site's `settings.php` file.
$settings['ckeditor5_extras_disabled_plugins'] = ['autoformat.Autoformat'];
```
